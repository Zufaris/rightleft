﻿public class Kata
{
  public static int FindEvenIndex(int[] arr)
  {
    if (arr.Length == 1 || (arr.Length > 1 && countSum(arr, 1, arr.Length-1) == 0)) {
      return 0;
    }

    if (arr.Length > 1 && countSum(arr, 0, arr.Length-2) ==0) {
      return arr.Length-1;
    }

    if (arr.Length == 2 && arr[0] == 0) {
      return 1;
    }

    int flag = -1;
    int i = 1;
    do  {
      int leftSum = countSum(arr, 0, i-1);
      int rightSum = countSum(arr, i+1, arr.Length-1);
      System.Console.WriteLine(i + "\t" +leftSum + "\t" + rightSum);
      if (leftSum == rightSum)  {
        flag = i;
      }
      i++;
    }
    while (flag < 0 & i < arr.Length-1);
    return flag;
  }

  static public int countSum(int [] arr1, int begin, int end)  {
    int Sum = 0;
    for (int k = begin; k <= end; k++)  {
      Sum += arr1[k];
    }
    return Sum;
  }

  public static void Main()  {
    int [] testArray = {3379,-3623,-637,5446,-4640,-7472,-6335,7784,-2911,1672,-6269,-756,7269,-7234,2488,-2128,641,2007,-1766,-4251,-5941,6721,2220,-3438,-654,1788,-8292,9767,-4434,-6834,478,7084,7261,316,-8909,1179,4497,5785,6044,-4976,959,8857,7800,-2114,-5900,-744,-6532,-2895,-1178,7043,2460,15,-2111,5877,8200,3776,1416,-9195,17255};
    System.Console.WriteLine(testArray.Length + "\t" + FindEvenIndex(testArray));
  }
}